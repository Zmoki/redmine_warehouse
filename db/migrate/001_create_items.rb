class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name, null: false
      t.float :price, null: false, default: 0
      t.integer :amount, null: false, default: 0
      t.datetime :manufacturing_date
      t.belongs_to :issue
      t.timestamps
    end
    add_index :items, :name, unique: true
  end
end
