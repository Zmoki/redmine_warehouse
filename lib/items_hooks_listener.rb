module RedmineWarehouse
  class ItemsHookListener < Redmine::Hook::ViewListener
    render_on :view_issues_show_description_bottom, :partial => '/items/issue'
  end
end
