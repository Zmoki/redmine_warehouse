require_dependency 'items_hooks_listener'

Redmine::Plugin.register :redmine_warehouse do
  name 'Redmine Warehouse plugin'
  author 'Alexander Gornov'
  description 'This is a storage plugin for Redmine'
  version '0.0.1'
  url 'none'
  author_url 'mailto:alexander.gornov@gmail.com'


  requires_redmine :version_or_higher => '2.1.2'
  requires_redmine_plugin :redmine_contacts, :version_or_higher => '3.2.14'
  project_module :warehouse do
    permission :items, { :items => :index, :markup => :index }, :public => true
    menu :project_menu, :items, {:controller => 'items', :action => 'index'}, :caption => :label_item, :before => :settings, :param => :project_id
    menu :project_menu, :markup, {:controller => 'markup', :action => 'index'}, :caption => :label_markup, :before => :items, :param => :project_id
    permission :add_operations, :items => [:new, :create]
    permission :edit_operations, :items => [:edit, :update]
    permission :delete_operations, :items => [:destroy]
    permission :view_operations, :items => [:index, :show]
  end

end
