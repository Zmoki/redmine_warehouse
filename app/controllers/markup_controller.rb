class MarkupController < ApplicationController
  unloadable
  before_filter :find_project
  private
  def find_project
    @project ||= if params[:project_id].present?
                   Project.find(params[:project_id])
                 else
                   find_issue.project
                 end
  end
  def index
  end
end
