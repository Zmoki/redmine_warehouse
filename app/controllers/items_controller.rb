class ItemsController < ApplicationController
  unloadable
  helper :sort
  include SortHelper
  before_filter :find_project
  before_filter :authorize, :only => :index
  before_filter :find_item, :except => [:index, :new, :create]
  #before_filter :find_issue, :only => [:new, :create]


  def index
   (render_403; return false) unless User.current.allowed_to?(:view_operations, @project)
    sort_init 'name', 'desc'
    sort_update %w(name price amount manufacturing_date created_at)
    @items = Items.of_project(@project)
    @items = @items.where("items.name like ?", "%#{params[:name]}%") if params[:name].present?
    @items = @items.order(sort_clause)
  end


  def new
    (render_403; return false) unless User.current.allowed_to?(:add_operations, @project)
    @item = Items.new
  end

  def edit
    (render_403; return false) unless User.current.allowed_to?(:edit_operations, @project)
    @item = Items. find(params[:id])
  end

  def create
    (render_403; return false) unless User.current.allowed_to?(:add_operations, @project)
    @item = Items.create(params[:item])
   if @item.save
      flash[:notice] = l(:notice_successful_save)
      redirect_to :action => 'show', :id => @item
    else
      render :action => 'new'
    end
  end

  def update
    (render_403; return false) unless User.current.allowed_to?(:edit_operations, @project)
    @item = Items.find(params[:id])
    @item.update_attributes(params[:item])
    if @item.save
      flash[:notice] = l(:notice_successful_update)
      redirect_to :action => 'show', :id => @item
    else
      render :action => 'edit'
    end
  end


  def destroy
    (render_403; return false) unless User.current.allowed_to?(:delete_operations, @project)
    @item = Items.find(params[:id])
    if @item.destroy
      flash[:notice] = l(:notice_successful_delete)
    else
      flash[:error] = l(:notice_unsuccessful_save)
    end
    respond_to do |format|
      format.html { redirect_back_or_default :action => "index", :project_id => params[:project_id] }

    end

  end

  def show
    (render_403; return false) unless User.current.allowed_to?(:view_operations, @project)
  end


  private

  def find_project
    @project = Project.find(params[:project_id])
  end

  def find_issue
    @issue ||= if params[:issue_id].present?
                 Issue.find(params[:issue_id])
               else
                 find_item.issue
               end
  end

  def find_item
    @item ||= Items.find(params[:id])
  end

end
