class Items < ActiveRecord::Base


  belongs_to :issue, class_name: 'Issue'#, :inverse_of => :products

  validates_presence_of :name, :price, :amount, :issue_id
  validates_numericality_of :price, greater_than_or_equal_to: 0
  validates_numericality_of :amount,  greater_than_or_equal_to: 0
  validates_numericality_of :issue_id, greater_than_or_equal_to: 0
  validates_uniqueness_of :name


  def project
    @project ||= issue.project
  end

  def self.of_project(project)
    self.joins(:issue).joins('INNER JOIN projects ON projects.id = issues.project_id').where('projects.id = ?', project.id)
  end

  def self.of_issue(issue)
    self.joins(:issue).where('issues.id = ?', issue.id)
  end
end
