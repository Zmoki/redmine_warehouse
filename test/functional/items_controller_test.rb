require File.expand_path('../../test_helper', __FILE__)

class ItemsControllerTest < ActionController::TestCase
  fixtures :projects,
           :users,
           :roles
  # Replace this with your real tests.
ActiveRecord::Fixtures.create_fixtures(File.dirname(__FILE__) + '/../fixtures/',
                            [:items,
                              :roles])

  def setup
    @controller = ItemsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    User.current = nil

  end


  test "shoud deny index" do
    get :index, :project_id => 1
    assert_response 403
  end

end
